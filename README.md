## Created by Carl Jägerhill

## How to get started
You have to clone the project from this repository: https://gitlab.com/mygroup1783/rpgconsolecharacters.git


## Gameplay
You'll start the game with name your character and choose your class depending which playstyle you want. Next up you'll get 3 choices. If you want to fight, show stats or leave the game. If you choose fight you will get a enemy with a random generated dps. If you win you will loot a random weapon or armor and also gain a level. If you lose it's game over.


## Leveling
Every time you level up you'll gain increased stats depending on which class you picked. 

There's 3 Main stats:
* Dexterity
* Inteligence
* Strength 


## Items
You'll recieve a random item each time you defeat a enemy. Each class has specific use of armor and weapons.
The items will yield random generated stats aswell.

* Rouge: Leather, Mail, Daggers and Swords
* Mage: Cloth, Wand and Staff
* Ranger: Leather, Mail, and Bow
* Warrior: Plate, Mail, Swords, Axe and Hammer

If you try to wear wrong type of armor and weapon you'll get a exception that is custom built.

