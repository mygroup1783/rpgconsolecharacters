﻿using RPGConsoleCharacters.Inventory;
using RPGConsoleCharacters.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGConsoleCharacters.Characters
{
    /// <summary>
    /// Parent class for all clasess
    /// </summary>
    public abstract class Character
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public PrimaryStat BaseStats { get; set; }
        public PrimaryStat TotalStats { get; set; }
        public double DPS { get; set; }
        public Dictionary<EquipmentSlot, Equipment> EQ { get; set; }

        /// <summary>
        /// Constructor that creates character
        /// </summary>
        /// <param name="name">Characters name</param>
        /// <param name="strength">Characters strength</param>
        /// <param name="dexterity">Characters dexterity</param>
        /// <param name="intelligence">Characters intelligence</param>

        public Character(string name, int strength, int dexterity, int intelligence)
        {
            Name = name;
            Level = 1;
            BaseStats = new PrimaryStat() { Strength = strength, Dexterity = dexterity, Intelligence = intelligence};
            EQ = new Dictionary<EquipmentSlot, Equipment>();
            CalcTotal();

        }

        /// <summary>
        /// Abstract method for levelup
        /// </summary>
        /// <param name="level">Characters level</param>
        public abstract void LevelUp(int level);

        /// <summary>
        /// Abstract method for equipping weapon
        /// </summary>
        /// <param name="weapon">Weapon</param>
        /// <returns>Return if the Character can use weapon or not</returns>
        public abstract string EquipWep(Weapons weapon);
        /// <summary>
        /// Abstract method for eqipping armor
        /// </summary>
        /// <param name="armor">armor</param>
        /// <returns>Return if the character can use armor or not</returns>
        public abstract string EquipArmor(Armor armor);

        /// <summary>
        /// Calculating total stats and writing the stats
        /// </summary>
        public void DisplayStats()
        {
            CalcTotal();

            WriteStats(Name, Level, TotalStats, DPS);
        }


        /// <summary>
        /// Calculats the characters DPS(Damage per second)
        /// </summary>
        /// <returns>The characters DPS</returns>
        public abstract double CalcDPS();

        /// <summary>
        /// Calculates the total stats on the equipment stats
        /// </summary>
        public void CalcTotal()
        {
            BaseStats = CalcEquipment();
            DPS = CalcDPS();
        }

        /// <summary>
        /// Calculating the bonuses you'll get when you equip armor
        /// </summary>
        /// <returns></returns>
        public PrimaryStat CalcEquipment()
        {
            PrimaryStat equipmentIncreaseStat = new() { Dexterity = 0, Intelligence = 0, Strength = 0 };

            bool hasLegEquipped = EQ.TryGetValue(EquipmentSlot.SLOT_LEGS, out Equipment legPiece);
            bool hasBodyEquipped = EQ.TryGetValue(EquipmentSlot.SLOT_BODY, out Equipment bodyPiece);
            bool hasHeadEquipped = EQ.TryGetValue(EquipmentSlot.SLOT_HEADER, out Equipment headPiece);

            if (hasLegEquipped)
            {
                Armor a = (Armor)legPiece;
                equipmentIncreaseStat += new PrimaryStat() { Dexterity = a.Stats.Dexterity, Intelligence = a.Stats.Intelligence, Strength = a.Stats.Strength };
            }
            if (hasBodyEquipped)
            {
                Armor a = (Armor)bodyPiece;
                equipmentIncreaseStat += new PrimaryStat() { Dexterity = a.Stats.Dexterity, Intelligence = a.Stats.Intelligence, Strength = a.Stats.Strength };
            }
            if (hasHeadEquipped)
            {
                Armor a = (Armor)headPiece;
                equipmentIncreaseStat += new PrimaryStat() { Dexterity = a.Stats.Dexterity, Intelligence = a.Stats.Intelligence, Strength = a.Stats.Strength };
            }

            return BaseStats + equipmentIncreaseStat;

        }

        /// <summary>
        /// Calculating the weapon dps
        /// </summary>
        /// <returns></returns>
        public double CalcWeaponDPS()
        {
            Equipment weaponPiece;
            bool hasWeaponEquipped = EQ.TryGetValue(EquipmentSlot.SLOT_WEAPON, out weaponPiece);
            if (hasWeaponEquipped)
            {
                Weapons wep = (Weapons)weaponPiece;
                return wep.weaponStat.AttackSpeed * wep.weaponStat.Damage;
            }
            else
            {
                return 1;
            }
        }

        /// <summary>
        /// Printing method that print the stats
        /// </summary>
        /// <param name="name">Characters name</param>
        /// <param name="level">Characters current level</param>
        /// <param name="statsTotal">Characters current stats</param>
        /// <param name="dps">Characters current dps</param>
        public void WriteStats(string name, int level, PrimaryStat statsTotal, double dps)
        {
            Console.WriteLine(String.Format($"NAME:{name}\n"));
            Console.WriteLine(String.Format($"LEVEL:{level}\n"));
            Console.WriteLine(String.Format($"DPS:{dps:0.##}\n"));
            Console.WriteLine(String.Format($"STRENGTH:{statsTotal.Strength}\n"));
            Console.WriteLine(String.Format($"DEXTERITY:{statsTotal.Dexterity}\n"));
            Console.WriteLine(String.Format($"INTELLIGENCE:{statsTotal.Intelligence}\n"));
        }








    }
}
