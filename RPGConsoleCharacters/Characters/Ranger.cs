﻿using RPGConsoleCharacters.ExceptionMessages;
using RPGConsoleCharacters.Inventory;
using RPGConsoleCharacters.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGConsoleCharacters.Characters
{
    /// <summary>
    /// Class type of Ranger
    /// </summary>
    public class Ranger : Character
    {
        /// <summary>
        /// Constructor creates a Ranger with base stats
        /// </summary>
        /// <param name="name"></param>
        public Ranger(string name) :
            base(name, 1, 7, 1)
        { }

        /// <summary>
        /// Inherrited from Character
        /// Method for Ranger levelup
        /// </summary>
        /// <param name="level">Character level</param>
        /// <exception cref="ArgumentException">If Ranger is below lvl 1 a exception will be thrown</exception>
        public override void LevelUp(int level)
        {
            if (level < 1)
            {
                throw new ArgumentException();
            }

            PrimaryStat newValues = new() { Dexterity = 5 * level, Intelligence = 1 * level, Strength = 1 * level };

            BaseStats += newValues;
            Level += 1 * level;

            CalcTotal();
        }


        /// <summary>
        /// Inherrited from Character.
        /// Calculating DPS
        /// </summary>
        /// <returns>DPS * characterDamage</returns>
        public override double CalcDPS()
        {
            TotalStats = CalcEquipment();
            double DPS = CalcWeaponDPS();

            if (DPS == 1)
            {
                return 1;
            }

            double characterDamage = 1 + TotalStats.Dexterity / 100;

            return DPS * characterDamage;
        }

        /// <summary>
        /// Inherrited from Character.
        /// Checking if Ranger can use the weapon
        /// </summary>
        /// <param name="weapon">Weapon</param>
        /// <returns>a string of weapon equipped</returns>
        /// <exception cref="InvalidWeaponEx">Throws an exception if the Ranger doesnt meet the requirments</exception>
        public override string EquipWep(Weapons weapon)
        {
            if(weapon.EquipmentLevel > Level)
            {
                throw new InvalidWeaponEx($"CHARACTER NEEDS TO BE LEVEL TO EQUIP THIS WEAPON: {weapon.EquipmentLevel}");
            }
            if(weapon.weaponType != WeaponType.WEAPON_BOW)
            {
                throw new InvalidWeaponEx($"CHARACTER CANT EQUIP THIS TYPE OF WEAPON: {weapon.weaponType}");
            }

            EQ[weapon.EquipmentSlot] = weapon;
            return "CONGRATULATIONS, WEAPON IS EQUIPPED!";
        }

        /// <summary>
        /// Inherrited from Character.
        /// Checking if Ranger can use the armor
        /// </summary>
        /// <param name="armor">armor</param>
        /// <returns>a string of armor equipped</returns>
        /// <exception cref="InvalidWeaponEx">Throws an exception if the Ranger doesnt meet the requirments</exception>
        public override string EquipArmor(Armor armor)
        {
            if(armor.EquipmentLevel > Level)
            {
                throw new InvalidArmorEx($"CHARACTER NEEDS TO BE LEVEL TO EQUIP THIS ARMOR: {armor.EquipmentLevel}");
            }
            if(armor.armorType != ArmorType.ARMOR_LEATHER && armor.armorType != ArmorType.ARMOR_MAIL)
            {
                throw new InvalidArmorEx($"CHARACTER CANT EQUIP THIS TYPE OF ARMOR: {armor.armorType}");
            }

            EQ[armor.EquipmentSlot] = armor;

            return "CONGRATULATIONS, ARMOR IS EQUIPPED!";
        }
    }
}
