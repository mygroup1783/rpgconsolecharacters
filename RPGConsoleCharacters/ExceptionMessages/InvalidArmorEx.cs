﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGConsoleCharacters.ExceptionMessages
{
    public class InvalidArmorEx : Exception
    {
        public InvalidArmorEx()
        {
        }

        public InvalidArmorEx(string message) : base(message)
        {
        }

        /// <summary>
        /// Function for unable to wear armor
        /// </summary>
        public override string Message => "Armor Exception ERROR";
    }
}
