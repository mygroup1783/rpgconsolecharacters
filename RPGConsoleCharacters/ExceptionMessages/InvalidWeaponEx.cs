﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGConsoleCharacters.ExceptionMessages
{
    public class InvalidWeaponEx : Exception
    {
        public InvalidWeaponEx()
        {
        }

        public InvalidWeaponEx(string message) : base(message)
        {
        }
        /// <summary>
        /// Function for unable to wear weapon
        /// </summary>
        public override string Message => "Weapon Exception Error";
    }
}
