﻿using RPGConsoleCharacters.Characters;
using RPGConsoleCharacters.Inventory;
using RPGConsoleCharacters.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGConsoleCharacters.GUI
{
    public class Fight
    {

        /// <summary>
        /// Message for invalid armor
        /// </summary>
        /// <param name="message">ErrorMessage</param>
        public static void EQErrorMessage(string message)
        {
            Console.WriteLine($"\n You cannot equip this item because wrong class: {message}");
        }

        /// <summary>
        /// Creating a enemy with random charactertype & Weapon
        /// </summary>
        /// <param name="level">Level of the character you created</param>
        /// <returns>A enemy with random weapon and damage</returns>

        public static Character CreateRandomEnemy(int level)
        {
            var rng = new Random();
            int type = rng.Next(1, 5);
            int damage = rng.Next(1, level);
            double attackSpeed = rng.NextDouble() * (level - 0.1) + 0.1;

            Weapons weapon = new()
            {
                EquipmentName = "Weapon",
                EquipmentLevel = level,
                EquipmentSlot = EquipmentSlot.SLOT_WEAPON,
                weaponStat = new WeaponStat() { Damage = damage, AttackSpeed = attackSpeed }
            };

            Character enemy;
            string name = "enemy";

            switch (type)
            {
                default:
                case 1:
                    enemy = new Warrior(name);
                    weapon.weaponType = WeaponType.WEAPON_HAMMER;
                    break;
                case 2:
                    enemy = new Ranger(name);
                    weapon.weaponType = WeaponType.WEAPON_BOW;
                    break;
                case 3:
                    enemy = new Rogue(name);
                    weapon.weaponType = WeaponType.WEAPON_DAGGER;
                    break;
                case 4:
                    enemy = new Mage(name);
                    weapon.weaponType = WeaponType.WEAPON_STAFF;
                    break;
            }

            if (level > 1)
            {
                enemy.LevelUp(level - 1);
            }

            try
            {
                enemy.EquipWep(weapon);
            }
            catch (Exception e)
            {
                EQErrorMessage(e.Message);
            }

            enemy.CalcTotal();

            return enemy;
        }


        /// <summary>
        /// Handle the fight bewteen your Character and the enemy. 
        /// If you win you'll level up and have the possibility to loot
        /// If you lose you'll get a losing message and the program shuts down
        /// </summary>
        /// <param name="character">Character Object</param>
        /// <param name="enemy">Enemy based on Character</param>
        /// <param name="characterLuck">Characters luck</param>
        /// <param name="enemyLuck">Enemies luck</param>
        public static void FightEnemy(Character character, Character enemy, double characterLuck, double enemyLuck)
        {
            GameGUI.EnemiesMessage(enemy.GetType().Name, enemy.DPS);

            Console.WriteLine("\nPRESS WHATEVER KEY YOU WANT TO CONTINUE IF YOU WANT TO CONTINUE!:D");
            Console.ReadKey();
            Console.Clear();


            if (character.DPS * characterLuck < enemy.DPS * enemyLuck)
            {
                Console.WriteLine("\nYOU LOST YOU FOOL");
                Console.WriteLine($"\nTHANK YOU FOR PLAYING!");
                Environment.Exit(0);
                return;
            }

            Console.WriteLine("YOU WON, WHAT A ALPHA");
            Console.WriteLine("\nPRESS WHATEVER KEY YOU WANT TO CONTINUE IF YOU WANT TO CONTINUE!:D");
            Console.ReadKey();
            Console.Clear();

            character.LevelUp(1);
            GameGUI.DingMessage(character.Level);

            Console.WriteLine("\nPRESS WHATEVER KEY YOU WANT TO CONTINUE IF YOU WANT TO CONTINUE!:D");
            Console.ReadKey();
            Console.Clear();

            FoundItemHandler(character);
        }


        /// <summary>
        /// Creating a rng based item and asking if you wanna choose it or not.
        /// </summary>
        /// <param name="character">Your character object</param>
        private static void FoundItemHandler(Character character)
        {
            Equipment eq = CreateRNGItem();

            string eqType = eq.GetType().Name;
            GameGUI.FoundMessage(eq.ItemDescription());

            switch (GameGUI.GetFoundItem())
            {
                case 1:
                    try
                    {
                        string typeWep = new Weapons().GetType().Name;

                        if (eqType.Equals(typeWep))
                        {
                            character.EquipWep((Weapons)eq);
                        }
                        else
                        {
                            character.EquipArmor((Armor)eq);
                        }

                        GameGUI.EquipmentEquipped(eqType);
                        Console.WriteLine("\nPRESS WHATEVER KEY YOU WANT TO CONTINUE IF YOU WANT TO CONTINUE!:D");
                        Console.ReadKey();
                        Console.Clear();

                    }
                    catch (Exception e)
                    {
                        GameGUI.EquipmentEquippedError(e.Message);

                        Console.WriteLine("\nPRESS WHATEVER KEY YOU WANT TO CONTINUE IF YOU WANT TO CONTINUE!:D");
                        Console.ReadKey();
                        Console.Clear();
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Creates a item based on RNG. Either it's armor or weapon
        /// with different sets of attributes.
        /// </summary>
        /// <returns>A random weapon or armor</returns>
        private static Equipment CreateRNGItem()
        {
            var rng = new Random();
            int itemType = rng.Next(0, 2);

            Equipment eq;

            switch (itemType)
            {
                case 0:
                    int wepType = rng.Next(7);
                    int wepDamage = rng.Next(1, 11);
                    double attackSpeed = rng.NextDouble() * (2.0 - 0.1) + 0.1;

                    eq = new Weapons()
                    {
                        EquipmentName = "Weapon",
                        EquipmentLevel = 1,
                        EquipmentSlot = EquipmentSlot.SLOT_WEAPON,
                        weaponType = (WeaponType)wepType,
                        weaponStat = new WeaponStat() { Damage = wepDamage, AttackSpeed = attackSpeed}
                    };
                    break;
                default:
                    int eqSlot = rng.Next(3);
                    int armorType = rng.Next(4);
                    int intelligence = rng.Next(3);
                    int dexterity = rng.Next(3);
                    int strength = rng.Next(3);

                    eq = new Armor()
                    {
                        EquipmentName = "Armor",
                        EquipmentLevel = 1,
                        EquipmentSlot = (EquipmentSlot)eqSlot,
                        armorType = (ArmorType)armorType,
                        Stats = new PrimaryStat() { Dexterity = dexterity, Intelligence = intelligence, Strength = strength }
                    };
                    break;
            }
            return eq;
        }




    }


}
