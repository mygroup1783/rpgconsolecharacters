﻿using RPGConsoleCharacters.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGConsoleCharacters.Game
{
    /// <summary>
    /// Logic for the game.
    /// </summary>
    public class Game
    {
        public Character character { get; set; }
        private int NameMaxLength = 30;


        /// <summary>
        /// Starting the game when Play method is called.
        /// Asking for Character class and Character Name.
        /// Then sends you to the Game options method
        /// </summary>
        public void Play()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(@"
   _____   ___  ______ _      _____    ___ ______ _   _ _____ _   _ _____ _   _______ _____ _____ _ 
  /  __ \ / _ \ | ___ \ |    /  ___|  / _ \|  _  \ | | |  ___| \ | |_   _| | | | ___ \  ___/  ___| |
  | /  \// /_\ \| |_/ / |    \ `--.  / /_\ \ | | | | | | |__ |  \| | | | | | | | |_/ / |__ \ `--.| |
  | |    |  _  ||    /| |     `--. \ |  _  | | | | | | |  __|| . ` | | | | | | |    /|  __| `--. \ |
  | \__/\| | | || |\ \| |____/\__/ / | | | | |/ /\ \_/ / |___| |\  | | | | |_| | |\ \| |___/\__/ /_|
   \____/\_| |_/\_| \_\_____/\____/  \_| |_/___/  \___/\____/\_| \_/ \_/  \___/\_| \_\____/\____/(_)
                 ");

            int characterType = GUI.GameGUI.GetCharacterClass();
            string name = GUI.GameGUI.GetCharacterName(NameMaxLength);
            character = CreateCharacter(characterType, name);

            int beginGame;
            do
            {
                beginGame = GUI.GameGUI.GameOptions();
            } while (GamePlay(beginGame));
        }


        /// <summary>
        /// Creating class based on input from user
        /// </summary>
        /// <param name="CharacterType">Class</param>
        /// <param name="name">Name of you character</param>
        /// <returns></returns>
        private static Character CreateCharacter(int CharacterType = 1, string name = "carl")
        {
            switch (CharacterType)
            {
                default:
                case 1:
                    return new Mage(name);
                case 2:
                    return new Ranger(name);
                case 3:
                    return new Rogue(name);
                case 4:
                    return new Warrior(name);

            }
        }


        /// <summary>
        /// Calling the right case based on input
        /// </summary>
        /// <param name="option">option for user to pick</param>
        /// <returns>True if game continues and false if you end or lose</returns>
        private bool GamePlay(int option)
        {
            switch (option)
            {
                case 1:
                    Character enemy = GUI.Fight.CreateRandomEnemy(character.Level);
                    var rng = new Random();
                    double playerLuck = rng.NextDouble() * (1.4 - 1.0) + 1.0;
                    double enemieLuck = rng.NextDouble() * (1.2 - 1.0) + 1.0;
                    GUI.Fight.FightEnemy(character, enemy, playerLuck, enemieLuck);
                    break;
                case 2:
                    character.DisplayStats();
                    break;
                default:
                case 3:
                    Console.WriteLine("THANKS FOR PLAYING, BETTER LUCK NEXT TIME!");
                    return false;

            }
            return true;
        }


    }
}
