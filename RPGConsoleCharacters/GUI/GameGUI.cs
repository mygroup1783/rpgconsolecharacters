﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGConsoleCharacters.GUI
{
    class GameGUI
    {
        /// <summary>
        /// Asks player to name their character
        /// </summary>
        /// <param name="nameMaxLength">Max amount of chars in name</param>
        /// <returns>Name of the user choosed</returns>
        public static string GetCharacterName(int nameMaxLength)
        {
            Console.WriteLine("\nEnter a name for your character: ");
            string name = Console.ReadLine();

            while (!InputInvalid(name, nameMaxLength))
            {
                Console.Clear();
                MaxLengthMessage(nameMaxLength);
                Console.WriteLine("\nPlease enter your characters name: ");
                name = Console.ReadLine();
            }

            Console.Clear();
            return name;
        }

        /// <summary>
        /// Simple method for printing out a error message
        /// </summary>
        /// <param name="length">length of the input</param>
        public static void MaxLengthMessage(int length)
        {
            Console.WriteLine($"\n To much letters: {length}!!!!!!!");
        }

        /// <summary>
        /// Method that checks length of the name.
        /// If the name is empty.
        /// </summary>
        /// <param name="name">The users input</param>
        /// <param name="nameMaxLength">Max length of name</param>
        /// <returns></returns>
        private static bool InputInvalid(string name, int nameMaxLength)
        {
            bool validLength = name.Length < nameMaxLength;
            bool isNotFilled = name != "";
            bool IsValidName = name.Any(c => char.IsLetterOrDigit(c));

            return isNotFilled && IsValidName && validLength;
        }

        /// <summary>
        /// Method for getting users choice of class decided by input
        /// </summary>
        /// <returns>Users choice of class</returns>
        public static int GetCharacterClass()
        {
            DisplayCharacterOptions();
            var Input = Console.ReadLine();
            int characterType;

            while (!int.TryParse(Input, out characterType) || characterType < 1 || characterType > 4)
            {
                Console.Clear();
                NumberMessage(1, 4);
                DisplayCharacterOptions();
                Input = Console.ReadLine();

            }

            Console.Clear();
            return characterType;

        }

        /// <summary>
        /// Simple method for user to understand which digits he can type
        /// </summary>
        /// <param name="from">Start value</param>
        /// <param name="to">End value</param>
        public static void NumberMessage(int from, int to)
        {
            Console.WriteLine($"\nPlease enter a number between {from} and {to}");
        }



        /// <summary>
        /// Printing method to print Character options and to keep code clean
        /// </summary>
        public static void DisplayCharacterOptions()
        {
            Console.WriteLine("\nCHOOSE THE CLASS OF YOUR CHARACTER");
            Console.WriteLine("(1) | MAGE");
            Console.WriteLine("(2) | RANGER");
            Console.WriteLine("(3) | ROGUE");
            Console.WriteLine("(4) | WARRIOR\n");
        }

        /// <summary>
        /// Printing method to print game options and to it reusable.
        /// </summary>
        /// <returns></returns>
        public static int GameOptions()
        {
            Console.WriteLine("\nGAME OPTIONS!");
            Console.WriteLine("(1) | YOU WANNA FIGHT HUH?");
            Console.WriteLine("(2) | SHOW MY IMBA STATS");
            Console.WriteLine("(3) | YOU COWARD YOU WANNA EXIT THE GAME??\n");

            var optionsInput = Console.ReadLine();
            int option;

            while (!int.TryParse(optionsInput, out option) || option < 1 || option > 4)
            {
                Console.Clear();
                NumberMessage(1, 3);
                Console.WriteLine("\nGAME OPTIONS!");
                Console.WriteLine("(1) | YOU WANNA FIGHT HUH?");
                Console.WriteLine("(2) | SHOW MY IMBA STATS");
                Console.WriteLine("(3) | YOU COWARD YOU WANNA EXIT THE GAME??\n");
                optionsInput = Console.ReadLine();
            }
            Console.Clear();

            return option;

        }

        /// <summary>
        /// Getting the found item when character won his duel
        /// </summary>
        /// <returns>A option if he wanna keep the loot or throw it</returns>
        public static int GetFoundItem()
        {

            Console.WriteLine("\nWHAT DO YOU WANT TO DO?");
            Console.WriteLine("(1) | TRY TO EQUIP THIS GARBAGE LOOT!");
            Console.WriteLine("(2) | AINT WORTH MY TIME TO LOOT\n");


            var itemInput = Console.ReadLine();
            int itemAction;
            while (!int.TryParse(itemInput, out itemAction) || itemAction < 1 || itemAction > 4)
            {
                Console.Clear();
                NumberMessage(1, 2);


                Console.WriteLine("\nWHAT DO YOU WANT TO DO?");
                Console.WriteLine("1) | TRY TO EQUIP THIS GARBAGE LOOT!");
                Console.WriteLine("(2) | AINT WORTH MY TIME TO LOOT\n");


                itemInput = Console.ReadLine();
            }
            Console.Clear();
            return itemAction;
        }

        /// <summary>
        /// Simple method for printing out characters level when he levels up
        /// </summary>
        /// <param name="level">Characters level</param>
        public static void DingMessage(int level)
        {     
            Console.WriteLine($"\nCONGRATULATIONS, YOU LEVEL UP TO: {level}!!!!");
        }

        /// <summary>
        ///  Simple method for printing out Characters found item.
        /// </summary>
        /// <param name="itemDescription">items name</param>
        public static void FoundMessage(string itemDescription)
        {
            Console.WriteLine($"\nYOU FOUND: {itemDescription}!!!");
        }

        /// <summary>
        /// Simple method to printing out enemy class and dps
        /// </summary>
        /// <param name="type">Enemy type</param>
        /// <param name="dps">Enemy DPS</param>
        public static void EnemiesMessage(string type, double dps)
        {
            Console.WriteLine($"\nYOUR ENEMY ARE A: {type}, WITH HUGE DPS OF {dps:0.##}");
        }

        /// <summary>
        /// Simple method to print out equipment equipped!
        /// </summary>
        /// <param name="equipmentDescription">Description for equipment</param>
        public static void EquipmentEquipped(string equipmentDescription)
        {
            Console.WriteLine($"\n{equipmentDescription} EQUIPPED!");
        }

        /// <summary>
        /// Simple method print out error message when wrong type of EQ
        /// </summary>
        /// <param name="message">message</param>
        public static void EquipmentEquippedError(string message)
        {
            Console.WriteLine($"\nYOU CANT EQUIP THIS EQUIPMENT: {message}");
        }


    }
}
