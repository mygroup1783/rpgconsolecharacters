﻿using RPGConsoleCharacters.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGConsoleCharacters.Inventory
{
    /// <summary>
    /// List of the avaible armor type for the character
    /// </summary>
    public enum ArmorType
    {
        ARMOR_CLOTH,
        ARMOR_LEATHER,
        ARMOR_MAIL,
        ARMOR_PLATE
    }

    /// <summary>
    /// Properties for Armortype based on Equipment
    /// </summary>
    public class Armor : Equipment
    {
        public ArmorType armorType { get; set; }

        public PrimaryStat Stats { get; set; }

        /// <summary>
        /// Inherrited from Equipment
        /// Displaying the description
        /// </summary>
        /// <returns>A string with the type of the armor</returns>
        public override string ItemDescription()
        {
            return $"Armor of type {armorType}";
        }

    }
}
