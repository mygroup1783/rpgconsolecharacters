﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGConsoleCharacters.Inventory
{
    /// <summary>
    /// Lists of equipment slot that can be filled with equipment
    /// </summary>
    public enum EquipmentSlot
    {
        SLOT_HEADER,
        SLOT_BODY,
        SLOT_LEGS,
        SLOT_WEAPON
    }

    /// <summary>
    /// Base of equipment
    /// </summary>
   public abstract class Equipment 
    {
        public string EquipmentName { get; set; }
        public int EquipmentLevel { get; set; }
        public EquipmentSlot EquipmentSlot { get; set; }

        /// <summary>
        /// Abstract method that display the type of equipment
        /// </summary>
        /// <returns></returns>
        public abstract string ItemDescription();
    }
}
