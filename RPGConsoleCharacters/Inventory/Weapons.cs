﻿using RPGConsoleCharacters.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGConsoleCharacters.Inventory
{
    /// <summary>
    /// List with a avaible weapons
    /// </summary>
    public enum WeaponType
    {
        WEAPON_AXE,
        WEAPON_BOW,
        WEAPON_DAGGER,
        WEAPON_HAMMER,
        WEAPON_STAFF,
        WEAPON_SWORD,
        WEAPON_WAND
    }

    /// <summary>
    /// /// Properties for Weapontype based on Equipment
    /// </summary>
    public class Weapons : Equipment
    {
        public WeaponType weaponType { get; set; }

        public WeaponStat weaponStat { get; set; }

        /// <summary>
        /// Inherrited from Equipment
        /// Displaying the description
        /// </summary>
        /// <returns>A string with the type of the weapon</returns>
        public override string ItemDescription()
        {
            return $"Weapon of type {weaponType}";
        }

    }
}
