﻿using System;
using RPGConsoleCharacters.GUI;


namespace RPGConsoleCharacters
{
    class Program
    {
        /// <summary>
        /// Starts a new instance of the game
        /// and the runs the method Play
        /// </summary>
        static void Main(string[] args)
        {
            Game.Game game = new();
            game.Play();
        }
    }
}
