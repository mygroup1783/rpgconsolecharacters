﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGConsoleCharacters.Stats
{
    /// <summary>
    /// Stat class to set stats to a character
    /// </summary>
    public class PrimaryStat
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        /// <summary>
        /// checks if two PrimaryStats are equal
        /// </summary>
        /// <param name="obj">object to check if its equal</param>
        /// <returns>true if it is equal and false if not</returns>
        public override bool Equals(object obj)
        {
            return obj is PrimaryStat stat &&
                   Strength == stat.Strength &&
                   Dexterity == stat.Dexterity &&
                   Intelligence == stat.Intelligence;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Strength, Dexterity, Intelligence);
        }

        /// <summary>
        /// Adds the PrimaryStats together 
        /// </summary>
        /// <param name="a">PrimaryStat 1</param>
        /// <param name="b">PrimaryStat 2</param>
        /// <returns></returns>
        public static PrimaryStat operator +(PrimaryStat a, PrimaryStat b) => new()
        {
            Dexterity = a.Dexterity + b.Dexterity,
            Intelligence = a.Intelligence + b.Intelligence,
            Strength = a.Strength + b.Strength
        };
    }
}
