﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGConsoleCharacters.Stats
{
    /// <summary>
    /// Class to check WeaponStats
    /// </summary>
   public class WeaponStat
    {
        public double AttackSpeed { get; set; }
        public int Damage { get; set; }
    }
}
