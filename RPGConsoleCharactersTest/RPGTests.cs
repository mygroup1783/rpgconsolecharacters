using RPGConsoleCharacters.Inventory;
using RPGConsoleCharacters.Stats;
using RPGConsoleCharacters.Characters;
using System;
using Xunit;
using RPGConsoleCharacters.ExceptionMessages;

namespace RPGTest
{
    /// <summary>
    /// Test for Right armor type, leveling, stats upgrade, calculate dps, equipment
    /// </summary>
    public class RPGTest
    {

        /// <summary>
        /// Test items for testing
        /// </summary>

        Weapons testAxe = new Weapons()
        {
            EquipmentName = "Common axe",
            EquipmentLevel = 1,
            EquipmentSlot = EquipmentSlot.SLOT_WEAPON,
            weaponType = WeaponType.WEAPON_AXE,
            weaponStat = new WeaponStat() { Damage = 7, AttackSpeed = 1.1 }
        };

        Armor testPlateBody = new Armor()
        {
            EquipmentName = "Common plate body armor",
            EquipmentLevel = 1,
            EquipmentSlot = EquipmentSlot.SLOT_BODY,
            armorType = ArmorType.ARMOR_PLATE,
            Stats = new() { Strength = 1 }
        };

        Weapons testBow = new Weapons()
        {
            EquipmentName = "Common bow",
            EquipmentLevel = 1,
            EquipmentSlot = EquipmentSlot.SLOT_WEAPON,
            weaponType = WeaponType.WEAPON_BOW,
            weaponStat = new WeaponStat() { Damage = 12, AttackSpeed = 0.8 }
        };

        Armor testClothHead = new Armor()
        {
            EquipmentName = "Common cloth head armor",
            EquipmentLevel = 1,
            EquipmentSlot = EquipmentSlot.SLOT_HEADER,
            armorType = ArmorType.ARMOR_CLOTH,
            Stats = new PrimaryStat() { Intelligence = 5 }
        };


        /// <summary>
        /// Checking if you're able to equip a weapon with higher level on it.
        /// </summary>
        
        [Fact]
        public void EquipWep_TryToEquipHighLevelWeapon()
        {
            Warrior warrior = new("Warrior");
            testAxe.EquipmentLevel = 2;

            Assert.Throws<InvalidWeaponEx>(() => warrior.EquipWep(testBow));
        }
        /// <summary>
        /// Checking if you're able to equip a armor with higher level on it.
        /// </summary>
        [Fact]
        public void EquipArmor_TryToEquipHighLevelArmor()
        {
            Warrior warrior = new("Warrior");
            testPlateBody.EquipmentLevel = 2;


            Assert.Throws<InvalidArmorEx>(() => warrior.EquipArmor(testPlateBody));
        }

        /// <summary>
        /// Checking if you can wear wrong type of weapon for your class.
        /// </summary>
        [Fact]
        public void EquipWep_TryToEquipWrongWeaponType()
        {
            Warrior warrior = new("Warrior");

            Assert.Throws<InvalidWeaponEx>(() => warrior.EquipWep(testBow));
        }

        /// <summary>
        /// Checking if you can wear wrong type of armor for your type.
        /// </summary>
        [Fact]
        public void Equip_TryToEquipWrongArmorType()
        {
            Warrior warrior = new("Warrior");
       
            Assert.Throws<InvalidArmorEx>(() => warrior.EquipArmor(testClothHead));
        }

        /// <summary>
        /// Checking if we get a sucess message when we wear a weapon.
        /// </summary>
        [Fact]
        public void EquipWep_GetSucessMessageBack()
        {
            Warrior warrior = new("Warrior");
            string expected = "CONGRATULATIONS, WEAPON IS EQUIPPED!";
            
            string actual = warrior.EquipWep(testAxe);

            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Checking if we get a sucess message when we wear a new piece of armor.
        /// </summary>
        [Fact]
        public void EquipArmor_GetSucessMessageBack()
        {
            Warrior warrior = new("Warrior");
            string expected = "CONGRATULATIONS, ARMOR IS EQUIPPED!";

            
            string actual = warrior.EquipArmor(testPlateBody);

            
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Checking if the addition of BaseStats work.
        /// </summary>
        [Fact]
        public void AdditonAddprimarystats_ShouldEqualSumOfStats()
        {
            PrimaryStat lhs = new() { Strength = 1, Dexterity = 1, Intelligence = 5 };
            PrimaryStat rhs = new() { Strength = 9, Dexterity = 9, Intelligence = 5 };
            PrimaryStat expected = new() { Strength = 10, Dexterity = 10, Intelligence = 10 };

            PrimaryStat actual = lhs + rhs;

            Assert.Equal(expected, actual);
        }


        /// <summary>
        /// Calculating the damage of a warrior level 1.
        /// </summary>
        [Fact]
        public void CalcDPS_CalculateDPSOfWarriorLevelOne()
        {
            Warrior warrior = new("Warrior");
            double expected = 1 * (1 + (5 / 100));

            double actual = warrior.CalcDPS();

            Assert.Equal(expected, actual);

        }

        /// <summary>
        /// Checking the damage of a warrior level one and weapon equipped.
        /// </summary>
        [Fact]
        public void CalcDPS_CalculateWarriorDamageLvlOneAndWepEquipped()
        {
            Warrior warrior = new("Warrior");
            double expected = (7 * 1.1) * (1 + (5 / 100));

            warrior.EquipWep(testAxe);
            double actual = warrior.CalcDPS();

            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Checking the damage of a warrior level one with weapon and armor equipped.
        /// </summary>
        [Fact]
        public void CalcDPS_CalculateWarriorDamageLvlOneAndWepEquippedAndArmor()
        {
            Warrior warrior = new("Warrior");
            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100));

            
            warrior.EquipWep(testAxe);
            warrior.EquipArmor(testPlateBody);

            double actual = warrior.CalcDPS();


            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Checking if the levelup method works.
        /// </summary>
        [Fact]
        public void LvlUp_Class_Correct_Level()
        {
            Warrior warrior = new("Warrior");
            int expected = 2;

            warrior.LevelUp(1);
            int actual = warrior.Level;

            Assert.Equal(expected, actual);
        }


        /// <summary>
        /// Checking if level one warrior basestats is right.
        /// </summary>
        [Fact]
        public void LvlUp_Class_LevelOne_RightStats_Warrior()
        {
            Warrior warrior = new("BotWarrior");
            PrimaryStat expected = new() { Strength = 5, Dexterity = 2, Intelligence = 1 };
            
            PrimaryStat actual = warrior.BaseStats;

            Assert.Equal(expected, actual);

        }


        /// <summary>
        /// Checking if level one ranger basestats is right.
        /// </summary>
        [Fact]
        public void LvlUp_Class_LevelOne_RightStats_Ranger()
        {
            Ranger ranger = new("BotRanger");
            PrimaryStat expected = new() { Strength = 1, Dexterity = 7, Intelligence = 1 };

            PrimaryStat actual = ranger.BaseStats;

            Assert.Equal(expected, actual);

        }

        /// <summary>
        /// Checking if level one rogue basestats is right.
        /// </summary>
        [Fact]
        public void LvlUp_Class_LevelOne_RightStats_Rogue()
        {
            Rogue rogue = new("BotRogue");
            PrimaryStat expected = new() { Strength = 2, Dexterity = 6, Intelligence = 1 };

            PrimaryStat actual = rogue.BaseStats;

            Assert.Equal(expected, actual);

        }

        /// <summary>
        /// Checking if level one mage basestats is right.
        /// </summary>

        [Fact]
        public void LvlUp_Class_LevelOne_RightStats_Mage()
        {
            Mage mage = new("BotMage");
            PrimaryStat expected = new() { Strength = 1, Dexterity = 1, Intelligence = 8 };

            PrimaryStat actual = mage.BaseStats;

            Assert.Equal(expected, actual);

        }


        /// <summary>
        /// checking if mage gains right stats after levelup.
        /// </summary>
        [Fact]
        public void LvlUp_MageLeveling_RightStats()
        {
            Mage mage = new("Mage");
            PrimaryStat expected = new() { Strength = 2, Dexterity = 2, Intelligence = 13 };

            mage.LevelUp(1);
            PrimaryStat actual = mage.BaseStats;

            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// checking if ranger gains right stats after levelup.
        /// </summary>
        [Fact]
        public void LvlUp_RangerLeveling_RightStats()
        {
            Ranger ranger = new("Ranger");
            PrimaryStat expected = new() { Strength = 2, Dexterity = 12, Intelligence = 2 };

            ranger.LevelUp(1);
            PrimaryStat actual = ranger.BaseStats;

            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// checking if rogue gains right stats after levelup.
        /// </summary>
        [Fact]
        public void LvlUp_RogueLeveling_RightStats()
        {
            Rogue rogue = new("Rogue");
            PrimaryStat expected = new() { Strength = 3, Dexterity = 10, Intelligence = 2 };

            rogue.LevelUp(1);
            PrimaryStat actual = rogue.BaseStats;

            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// checking if warrior gains right stats after levelup.
        /// </summary>
        [Fact]
        public void LvlUp_WarriorLeveling_RightStats()
        {
            Warrior warrior = new("Warrior");
            PrimaryStat expected = new() { Strength = 8, Dexterity = 4, Intelligence = 2 };

            warrior.LevelUp(1);
            PrimaryStat actual = warrior.BaseStats;

            Assert.Equal(expected, actual);
        }
            

    }
}
